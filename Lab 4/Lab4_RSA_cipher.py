import math
import random


def generate_key_pair(p, q):
    n = p * q
    phi = (p - 1) * (q - 1)

    # Выбираем случайное число e, которое является взаимно простым с phi
    e = random.randrange(1, phi)
    gcd_result = math.gcd(e, phi)
    while gcd_result != 1:
        e = random.randrange(1, phi)
        gcd_result = math.gcd(e, phi)

    # Вычисляем число d, тчо e*d % phi == 1
    # => d = (k*phi +1) / e
    k = 1
    while (k*phi + 1) % e > 0:
        k += 1
    d = (k*phi + 1) // e

    # Возвращаем открытый и секретный ключи
    return (e, n), (d, p, q)


class Cipher:
    def __init__(self, p, q):
        self.public_key, self.private_key = generate_key_pair(p, q)

    def encrypt(self, s):
        # Разбиваем текст на блоки и шифруем каждый блок
        e, n = self.public_key
        ciphertext = [chr(pow(ord(char), e, n)) for char in s]
        return ''.join(ciphertext)

    def decrypt(self, s):
        # Расшифровываем каждый блок и объединяем их в исходный текст
        d, p, q = self.private_key
        n = p * q
        plaintext = [chr(pow(ord(char), d, n)) for char in s]
        return ''.join(plaintext)


if __name__ == '__main__':
    p = 61
    q = 53

    coder = Cipher(p, q)
    main_menu_txt = f"This is RSA cipher.\n" \
                    f"Available commands: encrypt, decrypt, exit.\n" \
                    f"P = {p}\n" \
                    f"Q = {q}\n" \
                    f"Public key = {coder.public_key}\n" \
                    f"Private key = {coder.private_key}\n" \
                    f"\n" \
                    f"Write command here: "
    command = None
    while command != "exit":
        command = input(main_menu_txt)
        print()
        if command == "encrypt":
            input_string = input("Write your message here: ", )
            print(f"encoded message is\n\"{coder.encrypt(input_string)}\"\n")
        elif command == "decrypt":
            input_string = input("Write your encoded message here: ")
            print(f"decoded message is\n\"{coder.decrypt(input_string)}\"\n")
        elif command != "exit":
            print("Unknown command!\n")

