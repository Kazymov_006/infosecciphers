A = 5
C = 3
T0 = 7
b = 5
M = 2 ** b


def xor_op(bin_line, gamma):
    sequence = ''
    for i, g in zip(bin_line, gamma):
        sequence += str((int(i) + int(g)) % 2)
    return sequence


def get_text_from_sequence(sequence):
    text = ''
    for index in range(0, len(sequence), b):
        chunk = sequence[index:index+b]
        text += chr(int(chunk, 2) + 96)
    return text


if __name__ == '__main__':
    # encryption
    input_text = "informationalsecurityisawesome"
    encoded = ""
    prev_gamma_code = T0
    print(f"A = 5\n"
          f"C = 3\n"
          f"T0 = 7\n"
          f"b = 5\n"
          f"M = {M}\n")
    print(f"text = {input_text}")
    for letter in input_text:
        letter_bin = "{0:b}".format(ord(letter) - 96)
        letter_bin = "0" * (b - len(letter_bin)) + letter_bin

        letter_gamma = (A * prev_gamma_code + C) % M
        letter_gamma_bin = "{0:b}".format(letter_gamma)
        letter_gamma_bin = "0" * (b - len(letter_gamma_bin)) + letter_gamma_bin
        # print(letter_bin, letter_gamma_bin, prev_gamma_code)
        prev_gamma_code = letter_gamma_bin.count("1")
        encoded += xor_op(letter_bin, letter_gamma_bin)


    print(encoded)
    print(f"encoded = {get_text_from_sequence(encoded)}")

    prev_gamma_code = T0
    decoded = ""
    for letter in get_text_from_sequence(encoded):
        letter_bin = "{0:b}".format(ord(letter) - 96)
        letter_bin = "0" * (b - len(letter_bin)) + letter_bin

        letter_gamma = (A * prev_gamma_code + C) % M
        letter_gamma_bin = "{0:b}".format(letter_gamma)
        letter_gamma_bin = "0" * (b - len(letter_gamma_bin)) + letter_gamma_bin
        # print(letter_bin, letter_gamma_bin, prev_gamma_code)
        prev_gamma_code = letter_gamma_bin.count("1")
        decoded += xor_op(letter_bin, letter_gamma_bin)

    print(decoded)
    print(f"decoded = {get_text_from_sequence(decoded)}")


