def encrypt(message):
    cleaned_msg = message

    if len(cleaned_msg) % KEY_LEN != 0:
        cleaned_msg += '0' * (KEY_LEN - len(cleaned_msg) % KEY_LEN)

    enc_msg = ''
    for i in range(0, len(cleaned_msg), KEY_LEN):
        for j in range(KEY_LEN):
            enc_msg += cleaned_msg[i + key[j % KEY_LEN] - 1]

    enc_msg_plain = ''
    for i in range(KEY_LEN):
        for j in range(len(enc_msg) // KEY_LEN):
            enc_msg_plain += enc_msg[i + KEY_LEN * j]

    return enc_msg_plain


def decrypt(cipher_text):
    CT_LEN = len(cipher_text)

    dec_msg = ''
    for i in range(CT_LEN // KEY_LEN):
        for j in range(KEY_LEN):
            dec_msg += cipher_text[i + (CT_LEN // KEY_LEN) * j]

    plain_text = ''
    for i in range(0, CT_LEN, KEY_LEN):
        for j in range(KEY_LEN):
            plain_text = plain_text + dec_msg[i + dkey[j % KEY_LEN] - 1]

    plain_text = plain_text.replace("0", "")
    return plain_text


if __name__ == '__main__':
    key = list(map(int, "3 1 4 5 2".split()))
    msg = "INFORMATIONAL SECURITY IS AWESOME"
    print(f"message = {msg}")

    KEY_LEN = len(key)
    print(key)
    dkey = [key.index(i) + 1 for i in range(1, KEY_LEN + 1)]
    print(dkey)

    encrypted_text = encrypt(msg)
    print(f"encoded = {encrypted_text}")

    decrypted_text = decrypt(encrypted_text)
    print(f"decoded = {decrypted_text}")
