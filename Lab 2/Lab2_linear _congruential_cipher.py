a = 5
b = 6
c = 3
t0 = 7


class LinearCongruentialGenerator:
    def __init__(self, a, b, c, t0):
        self.a = a
        self.b = b
        self.c = c
        self.m = int(2**b)
        self.t0 = t0

    def encrypt(self, s):
        assert len(s) % self.b == 0
        k = len(s) // self.b
        t = [self.t0]
        for i in range(k):
            t.append((self.a*t[i] + self.c) % self.m)
        answer = []
        for i in range(k):
            chunk = s[i*self.b:(i+1)*self.b]
            dec = int(chunk, 2)
            enc_dec = dec ^ t[i+1]  # XOR
            enc_bin = f"{enc_dec:06b}"
            answer.append(enc_bin)
        return "".join(answer)


    def decrypt(self, s):
        return self.encrypt(s)




if __name__ == '__main__':
    coder = LinearCongruentialGenerator(a, b, c, t0)
    main_menu_txt = f"This is Linear Congruential cipher.\n" \
                    f"Available commands: encrypt, decrypt, exit.\n" \
                    f"A = {coder.a}\n" \
                    f"B = {coder.b}\n" \
                    f"C = {coder.c}\n" \
                    f"T0 = {coder.t0}\n" \
                    f"M = {coder.m}\n" \
                    f"\n" \
                    f"Write command here: "
    command = None
    while command != "exit":
        command = input(main_menu_txt)
        print()
        if command == "encrypt":
            input_string = input("Write your message here: ", )
            print(f"encoded message is\n\"{coder.encrypt(input_string)}\"\n")
        elif command == "decrypt":
            input_string = input("Write your encoded message here: ")
            print(f"decoded message is\n\"{coder.decrypt(input_string)}\"\n")
        elif command != "exit":
            print("Unknown command!\n")

