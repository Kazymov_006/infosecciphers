permutation_table = [4, 3, 0, 1, 2]
fill_symbol = " "
k = 5


class Cipher:
    def __init__(self, pt, fs):
        self.permutation_table = pt
        self.fill_symbol = fs
        self.k = len(self.permutation_table)
        id_map = zip(self.permutation_table, [i for i in range(self.k)])
        self.decrypt_permutation_table = list(map(lambda x: x[1], sorted(id_map, key=lambda x: x[0])))

    def encrypt(self, s):
        missing_n = (self.k - len(s) % self.k) % self.k
        missing_s = self.fill_symbol * missing_n
        s += missing_s

        answer = []
        i = 0
        s_as_list = list(s)
        for _ in s:
            answer.append(s_as_list[self.permutation_table[i]])
            i += 1
            i %= self.k
            if i == 0:
                for __ in range(self.k):
                    if s_as_list:
                        s_as_list.pop(0)
        return "".join(answer)

    def decrypt(self, s):
        answer = []
        i = 0
        s_as_list = list(s)
        for _ in s:
            answer.append(s_as_list[self.decrypt_permutation_table[i]])
            i += 1
            i %= self.k
            if i == 0:
                for __ in range(self.k):
                    if s_as_list:
                        s_as_list.pop(0)
        return "".join(answer)


if __name__ == '__main__':
    coder = Cipher(permutation_table, " ")
    main_menu_txt = f"This is permutation cipher.\n" \
                    f"Available commands: encrypt, decrypt, exit.\n" \
                    f"Permutation matrix = {coder.permutation_table}\n" \
                    f"Decrypt permutation matrix = {coder.decrypt_permutation_table}\n" \
                    f"\n" \
                    f"Write command here: "
    command = None
    while command != "exit":
        command = input(main_menu_txt)
        print()
        if command == "encrypt":
            input_string = input("Write your message here: ", )
            print(f"encoded message is\n\"{coder.encrypt(input_string)}\"\n")
        elif command == "decrypt":
            input_string = input("Write your encoded message here: ")
            print(f"decoded message is\n\"{coder.decrypt(input_string)}\"\n")
        elif command != "exit":
            print("Unknown command!\n")

