rounds = 8
secret_key = "IB"
fill_symbol = " "

alphabet = enumerate("abcdefghijklmnopqrstuvwxyz"+"abcdefghijklmnopqrstuvwxyz".upper()+" 0123456789.")
al2code = {}
code2al = {}
for k, v in alphabet:
    al2code[v] = k
    code2al[k] = v

def xor(text1, text2):
    result = []
    for i in range(len(text1)):
        # Вычисляем XOR для символов двух текстов
        xored = al2code[text1[i]] ^ al2code[text2[i]]
        # Добавляем полученный символ в результат
        result.append(code2al[xored])
    return "".join(result)


def add_function(text, key):
    result = []
    for i in range(len(text)):
        # Вычисляем сумму символов текста и ключа по модулю 64
        sum = (al2code[text[i]] ^ al2code[key])
        # Добавляем полученный символ в результат
        result.append(code2al[sum])
    return "".join(result)


class FeistelCipher:
    def __init__(self, rounds, secret_key, fs):
        self.rounds = rounds
        self.key = secret_key
        self.fill_symbol = fs

    def _crypt(self, text_left, text_right, K1, K2):
        # Разбиваем входной текст на две части
        left, right = text_left, text_right
        K1 = f"{al2code[K1]:06b}"
        K2 = f"{al2code[K2]:06b}"

        # Проходим по указанному количеству раундов
        for i in range(self.rounds):
            # Вычисляем ключ для текущего раунда
            K1ROLi = int(K1[i % 6:] + K1[:i % 6], 2)
            K2RORi = int(K2[-i % 6:] + K2[:-i % 6], 2)
            round_key = code2al[K1ROLi ^ K2RORi]
            # round_key = code2al[al2code[K1] ^ al2code[K2]]

            # Применяем функцию F к правой части текста и ключу
            f_result = add_function(left, round_key)

            # Вычисляем новое значение правой части текста
            new_left = xor(f_result, right)

            # Перемещаем правую часть текста в левую, а новое значение правой части в правую
            left, right = new_left, left

        # Объединяем левую и правую части текста и возвращаем результат
        return left, right


    def encrypt(self, text):
        K1, K2 = self.key[0], self.key[1]
        if len(text) % 2 > 0:
            text += self.fill_symbol
        text_left, text_right = text[:len(text) // 2], text[len(text) // 2:]
        l, r = self._crypt(text_left, text_right, K1, K2)
        return l+r


    def decrypt(self, text):
        # Инвертируем порядок ключей
        K1, K2 = f"{al2code[self.key[1]]:06b}", f"{al2code[self.key[0]]:06b}"
        K1, K2 = code2al[int(K1[-7 % 6:] + K1[:-7 % 6], 2)], code2al[int(K2[7 % 6:] + K2[:7 % 6], 2)]
        if len(text) % 2 > 0:
            text += self.fill_symbol
        text_left, text_right = text[len(text) // 2:], text[:len(text) // 2]
        l, r = self._crypt(text_left, text_right, K1, K2)
        return r+l


if __name__ == '__main__':
    coder = FeistelCipher(rounds, secret_key, " ")
    main_menu_txt = f"This is Feistel cipher.\n" \
                    f"Available commands: encrypt, decrypt, exit.\n" \
                    f"Generating function - Add\n" \
                    f"Rounds = {coder.rounds}\n" \
                    f"Secret key = {coder.key}\n" \
                    f"\n" \
                    f"Write command here: "
    command = None
    while command != "exit":
        command = input(main_menu_txt)
        print()
        if command == "encrypt":
            input_string = input("Write your message here: ", )
            print(f"encoded message is\n\"{coder.encrypt(input_string)}\"\n")
        elif command == "decrypt":
            input_string = input("Write your encoded message here: ")
            print(f"decoded message is\n\"{coder.decrypt(input_string)}\"\n")
        elif command != "exit":
            print("Unknown command!\n")

