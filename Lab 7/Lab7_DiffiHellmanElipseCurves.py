def is_prime(number: int) -> bool:
    primes = [i for i in range(number + 1)]
    primes[1] = 0
    for i in range(2, number + 1):
        if primes[i] != 0:
            for j in range(i * 2, number + 1, i):
                primes[j] = 0
    return primes[number] != 0


def multiplicative_inverse(a, m):
    return pow(a, -1, m)


def double(x1, y1, p, a):
    s = ((3 * (x1 ** 2) + a) * multiplicative_inverse(2 * y1, p)) % p
    x3 = (s ** 2 - x1 - x1) % p
    y3 = (s * (x1 - x3) - y1) % p
    return x3, y3


def add(x1, y1, x2, y2, p, a):
    s = 0
    if x1 == x2:
        s = ((3 * (x1 ** 2) + a) * multiplicative_inverse(2 * y1, p)) % p
    else:
        s = ((y2 - y1) * multiplicative_inverse(x2 - x1, p)) % p
    x3 = (s ** 2 - x1 - x2) % p
    y3 = (s * (x1 - x3) - y1) % p
    return x3, y3


def double_and_add(scalar, point, p, a):
    x, y = point
    result = (0, 0)
    while scalar:
        if scalar & 1:
            result = add(*result, *point, p, a)
        point = double(*point, p, a)
        scalar >>= 1
    return result


def double_and_add(scalar, point, p, a):
    (x3, y3) = (0, 0)
    (x1, y1) = point
    (x_tmp, y_tmp) = point
    init = 0
    for i in str(bin(scalar)[2:]):
        if (i == '1') and (init == 0):
            init = 1
        elif (i == '1') and (init == 1):
            (x3, y3) = double(x_tmp, y_tmp, p, a)
            (x3, y3) = add(x1, y1, x3, y3, p, a)
            (x_tmp, y_tmp) = (x3, y3)
        else:
            (x3, y3) = double(x_tmp, y_tmp, p, a)
            (x_tmp, y_tmp) = (x3, y3)
    return x3, y3


if __name__ == '__main__':
    # a = int(input("a: "))
    # b = int(input("b: "))
    a = -1
    b = 1

    if 4 * (a ** 3) + 27 * (b ** 2) == 0:
        print("Невозможно построить кривую")
        exit()

    # p = int(input("p: "))
    p = 29
    if not is_prime(p):
        print("P должно быть простым")
        exit()

    # point = tuple(map(int, input("G(x, y): ").split()))
    point = (9, 27)

    if point[1]**2 == point[0]**3 + a*point[0] + b%p:
        print("точка не лежит на кривой")
        exit()

    # k1 = int(input("k1: "))
    # k2 = int(input("k2: "))
    k1 = 4
    k2 = 17

    K1 = double_and_add(k1, point, p, a)
    K2 = double_and_add(k2, point, p, a)
    print(K1, K2)

    publicK1 = double_and_add(k2, K1, p, a)
    publicK2 = double_and_add(k1, K2, p, a)
    print(
        f"a = {a}\n"
        f"b = {b}\n"
        f"p = {p}\n"
        f"point = {point}\n"
        f"k1 = {K1}\n"
        f"k2 = {K2}\n"
    )
    print(publicK1, publicK2)
